<?php

namespace We7\V2719;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1647853421
 * @version 2.7.19
 */

class UpdateWechatDraft {
	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('wechat_attachment', 'publish_id')) {
			pdo_run("ALTER TABLE `ims_wechat_attachment` ADD `publish_id` VARCHAR(10) UNSIGNED NOT NULL;");
		}
		if (!pdo_fieldexists('wechat_attachment', 'publish_status')) {
			pdo_run("ALTER TABLE `ims_wechat_attachment` ADD `publish_status` TINYINT(1) UNSIGNED NOT NULL;");
		}
		if (!pdo_fieldexists('wechat_attachment', 'article_id')) {
			pdo_run("ALTER TABLE `ims_wechat_attachment` ADD `article_id` VARCHAR(255) NOT NULL;");
		}
		if (!pdo_fieldexists('wechat_news', 'is_deleted')) {
			pdo_run("ALTER TABLE `ims_wechat_news` ADD `is_deleted` TINYINT(1) UNSIGNED NOT NULL;");
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {
	}
}
