<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1545305615
 * @version 1.8.8
 */

class InitSystemMenu {

	/**
	 *  执行更新
	 */
	public function up() {
		$condition = array('title' => '', 'group_name' => 'frame', 'icon' => '', 'url' => '', 'type' => 'url', 'is_system' => 1);
		pdo_delete('core_menu', $condition);

		$menu_data['is_display'] = 0;
		$menu_data['is_system'] = 1;
		$menu_data['group_name'] = 'frame';
		$undisplay_menu = array('custom_help', 'help');
		foreach ($undisplay_menu as $permission_name) {
			$menu_data['permission_name'] = $permission_name;
			pdo_insert('core_menu',  $menu_data);
		}

		cache_build_frame_menu();
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		