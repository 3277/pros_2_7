<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1546505315
 * @version 1.8.8
 */

class DeleteUnusedFile {

	/**
	 *  执行更新
	 */
	public function up() {
		$files = array(
			IA_ROOT . '/framework/class/account.class.php',
			IA_ROOT . '/framework/class/weixin.account.class.php',
			IA_ROOT . '/framework/class/weixin.platform.class.php',
			IA_ROOT . '/framework/class/wxapp.account.class.php',
			IA_ROOT . '/framework/class/webapp.account.class.php',
			IA_ROOT . '/framework/class/yixin.account.class.php',
			IA_ROOT . '/framework/class/pay.class.php',
			IA_ROOT . '/framework/class/weixin.pay.class.php',
			IA_ROOT . '/framework/class/ali.pay.class.php',
			IA_ROOT . '/framework/class/weixin.nativepay.php',
		);
		foreach ($files as $file) {
			if (file_exists($file)) {
				unlink($file);
			}
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		