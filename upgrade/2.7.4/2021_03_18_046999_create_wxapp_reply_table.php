<?php

namespace We7\V274;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1616046999
 * @version 2.7.4
 */

class CreateWxappReplyTable {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_tableexists('wxapp_reply')) {
			$table_name = tablename('wxapp_reply');
			$sql = <<<EOF
CREATE TABLE IF NOT EXISTS $table_name (
`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`rid` INT(10) UNSIGNED NOT NULL,
`title` VARCHAR(50) NOT NULL,
`appid` VARCHAR(50) NOT NULL,
`pagepath` VARCHAR(255) NOT NULL,
`mediaid` VARCHAR(50) NOT NULL,
`createtime` INT(10) NOT NULL,
PRIMARY KEY(`id`),
INDEX `rid` (`rid`) USING BTREE
) ENGINE=InnoDB;
EOF;
			pdo_query($sql);
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
