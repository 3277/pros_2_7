<?php
/**
 * 初始化web端数据
 * [WeEngine System] Copyright (c) 2014 W7.CC.
 */
defined('IN_IA') or exit('Access Denied');

load()->web('common');
load()->web('template');
load()->func('file');
load()->func('tpl');
load()->model('cloud');
load()->model('user');
load()->model('permission');
load()->model('attachment');
load()->classs('oauth2/oauth2client');
load()->model('switch');
load()->model('system');

$_W['isw7_request'] = !empty($_SERVER['HTTP_W7_OAUTHTOKEN']) || isset($_GPC['w7_request_secret']) && isset($_GPC['w7_accesstoken']);
$_W['isw7_sign'] = (isset($_GPC['w7_sign']) || igetcookie('__w7sign')) && !empty($_SERVER['HTTP_SEC_FETCH_DEST']) && $_SERVER['HTTP_SEC_FETCH_DEST'] != 'document';
if (!empty($_SERVER['HTTP_W7_OAUTHTOKEN'])) {
	$_GPC['__session'] = $_SERVER['HTTP_W7_OAUTHTOKEN'];
	$_W['isw7_request'] = true;
}
if ($_W['isw7_sign']) {
	if (!empty($_GPC['w7_sign']) && safe_gpc_string($_GPC['w7_sign']) != igetcookie('__w7sign')) {
		isetcookie('__w7sign', safe_gpc_string($_GPC['w7_sign']));
	}
	$_GPC['__session'] = igetcookie('__w7sign');
}

if (isset($_GPC['w7_accesstoken']) && isset($_GPC['w7_request_secret'])) {
	$request_token = cloud_w7_request_token(safe_gpc_string($_GPC['w7_request_secret']));
	if ($_GPC['w7_accesstoken'] != $request_token) {
		$request_token = cloud_w7_request_token(safe_gpc_string($_GPC['w7_request_secret']), true);
	}
	if (is_error($request_token)) {
		$onlineSecret = file_get_contents('https://cdn.w7.cc/ued/we7/release/js/getSecret.js?r=' . TIMESTAMP);
		WeUtility::logging('cloud-api-error', array('secret' => $_GPC['w7_request_secret'], 'remote_secret' => $onlineSecret, 'accesstoken' => $_GPC['w7_accesstoken'], 'request_token' => $request_token), true);
		iajax(403, '错误详情:' . $request_token['message']);
	}
	if (!empty($request_token) && $_GPC['w7_accesstoken'] == $request_token) {
		$record = pdo_get('users', array('uid' => current(explode(',', $_W['config']['setting']['founder']))));
		$record['hash'] = md5($record['password'] . $record['salt']);
		$_GPC['__session'] = authcode(json_encode(array('uid' => $record['uid'], 'hash' => $record['hash'], 'lastip' => $record['lastip'], 'lastvisit' => $record['lastvisit'])), 'encode');
		pdo_update('users', array('lastvisit' => TIMESTAMP, 'lastip' => $_W['clientip']), array('uid' => $record['uid']));
		unset($record);
	}
}
$session = !empty($_GPC['__session']) ? json_decode(authcode($_GPC['__session']), true) : '';
if (is_array($session)) {
	$user = user_single(array('uid' => $session['uid']));
	if (is_array($user) && $session['hash'] === $user['hash']) {
		$_W['uid'] = $user['uid'];
		$_W['username'] = $user['username'];
		$user['currentvisit'] = $user['lastvisit'];
		$user['currentip'] = $user['lastip'];
		$user['lastvisit'] = empty($session['lastvisit']) ? '' : $session['lastvisit'];
		$user['lastip'] = empty($session['lastip']) ? '' : $session['lastip'];
		$_W['user'] = $user;
		$_W['isfounder'] = user_is_founder($_W['uid']);
		$_W['isadmin'] = user_is_founder($_W['uid'], true);
	} else {
		isetcookie('__session', '', -100);
	}
	unset($user);
}
unset($session);
if (!empty($_W['uid']) && !empty($_SERVER['HTTP_SEC_FETCH_DEST']) && 'iframe' == $_SERVER['HTTP_SEC_FETCH_DEST'] && !empty($_W['user']['bind_domain_id'])) {
	$cannon_fodder_domain = pdo_get('cannon_fodder', array('id' => $_W['user']['bind_domain_id']), array('domain'));
	if (!empty($cannon_fodder_domain['domain']) && strpos($_W['siteroot'], $cannon_fodder_domain['domain']) === false) {
		$tourl = $cannon_fodder_domain['domain'] . (false === strpos($_SERVER['REQUEST_URI'], '?') ? ($_SERVER['REQUEST_URI'] . '?') : $_SERVER['REQUEST_URI']) . '&w7_sign=' . rawurlencode($_GPC['__session']);
		header('Location: ' . $tourl);
		exit();
	}
}
if (empty($_GPC['w7i']) && !empty($_GPC['uniacid']) && 0 < $_GPC['uniacid']) {
	$_GPC['w7i'] = $_GPC['uniacid'];
}
$_W['uniacid'] = !empty($_GPC['w7i']) ? $_GPC['w7i'] : igetcookie('__uniacid');
if (empty($_W['uniacid'])) {
	$_W['uniacid'] = switch_get_account_display();
}
$_W['uniacid'] = $_GPC['w7i'] = intval($_W['uniacid']);
if (!empty($_GPC['w7i']) && !empty(igetcookie('__uniacid')) && $_GPC['w7i'] != igetcookie('__uniacid')) {
	isetcookie('__uniacid', $_W['uniacid'], 7 * 86400);
}

if (!empty($_W['uid'])) {
	$_W['highest_role'] = permission_account_user_role($_W['uid']);
	$_W['role'] = permission_account_user_role($_W['uid'], $_W['uniacid']);
}

$_W['template'] = '2.0';

$_W['token'] = token();
$_W['attachurl'] = attachment_set_attach_url();
