<?php
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * $sn$.
 */
defined('IN_IA') or exit('Access Denied');
load()->model('article');
load()->model('module');

if (!empty($_W['uid'])) {
	header('Location: ' . $_W['siteroot'] . 'web/home.php');
	exit;
}

/*获取站点配置信息*/
$settings = $_W['setting'];

$copyright = $settings['copyright'];
$copyright['slides'] = iunserializer($copyright['slides']);
if (isset($copyright['showhomepage']) && empty($copyright['showhomepage'])) {
	header('Location: ' . url('user/login'));
	exit;
}
if (!empty($_W['setting']['copyright']['local_install'])) {
	$login_url = url('user/login');
} else {
	$cloud_console_invite_url = cloud_console_invite_url();
	if (!empty($_W['setting']['app_redirect']) && safe_gpc_url($_W['setting']['app_redirect'])) {
		$login_url = $_W['siteroot'] . '?w7_app_redirect=1#' . $cloud_console_invite_url['hash_url'];
	} else {
		$login_url = $cloud_console_invite_url['share_url'];
	}
}
$notices = article_notice_home();
$news = article_news_home();
template('account/welcome');
